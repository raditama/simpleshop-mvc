package com.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleshopMVCApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleshopMVCApplication.class, args);
	}

}
