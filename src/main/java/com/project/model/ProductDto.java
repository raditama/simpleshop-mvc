package com.project.model;

import lombok.Data;

@Data
public class ProductDto {
	private Long id;
	private String name;
	private String code;
	private Integer stock;
	private String description;
	private Integer price;

	public ProductDto() {
		super();
	}

	public ProductDto(Long id, String name, String code, Integer stock, String description, Integer price) {
		super();
		this.id = id;
		this.name = name;
		this.code = code;
		this.stock = stock;
		this.description = description;
		this.price = price;
	}

	public ProductDto(String name, String code, Integer stock, String description, Integer price) {
		super();
		this.name = name;
		this.code = code;
		this.stock = stock;
		this.description = description;
		this.price = price;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

}
