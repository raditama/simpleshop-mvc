package com.project.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.project.model.Product;

public interface ProductRepo extends JpaRepository<Product, Long> {
	@Modifying
	@Transactional
	@Query(value = "UPDATE product SET stock = stock - :quantity WHERE code = :code", nativeQuery = true)
	void updateStockProduct(@Param("quantity") int quantity, @Param("code") String code);
}
