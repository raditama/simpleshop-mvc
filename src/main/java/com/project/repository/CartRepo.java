package com.project.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.project.model.Cart;

public interface CartRepo extends JpaRepository<Cart, Long> {
	@Query(value = "SELECT * FROM cart WHERE code = :code", nativeQuery = true)
	Optional<Cart> findByCode(@Param("code") String code);

}
