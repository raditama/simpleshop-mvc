package com.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.model.Users;

public interface UserRepo extends JpaRepository<Users, Long> {

}
