package com.project.restcontroller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.model.Cart;
import com.project.repository.CartRepo;
import com.project.repository.ProductRepo;

@RestController
@RequestMapping("/api/cart")
@CrossOrigin(origins = "*")
public class CartController {
	@Autowired
	CartRepo cartRepo;

	@Autowired
	ProductRepo productRepo;

	@GetMapping("/get")
	public ResponseEntity<List<Cart>> getAllCart() {
		try {
			List<Cart> cart = new ArrayList<Cart>();

			cartRepo.findAll().forEach(cart::add);

			if (cart.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(cart, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/post")
	public ResponseEntity<Cart> createCart(@RequestBody Cart cart) {
		try {
			Cart _cart = cartRepo.save(new Cart(cart.getName(), cart.getCode(), cart.getQuantity(), cart.getPrice()));
			return new ResponseEntity<>(_cart, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/put/{id}")
	public ResponseEntity<Cart> updateCart(@PathVariable("id") Long id, @RequestBody Cart cart) {
		Optional<Cart> cartData = cartRepo.findById(id);

		if (cartData.isPresent()) {
			Cart _cart = cartData.get();
			_cart.setQuantity(cart.getQuantity());

			return new ResponseEntity<>(cartRepo.save(_cart), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/del/{id}")
	public ResponseEntity<HttpStatus> deleteCart(@PathVariable("id") Long id) {
		try {
			cartRepo.deleteById(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/add")
	public ResponseEntity<Cart> addCart(@RequestBody Cart cart) {
		Optional<Cart> cartData = cartRepo.findByCode(cart.getCode());

		if (cartData.isPresent()) {
			Cart _cart = cartData.get();
			_cart.setName(cart.getName());
			_cart.setCode(cart.getCode());
			_cart.setQuantity(_cart.getQuantity() + 1);
			_cart.setPrice(cart.getPrice());

			return new ResponseEntity<>(cartRepo.save(_cart), HttpStatus.OK);
		} else {
			Cart _cart = cartRepo.save(new Cart(cart.getName(), cart.getCode(), 1, cart.getPrice()));
			return new ResponseEntity<>(_cart, HttpStatus.CREATED);
		}
	}

	@PutMapping("/checkout")
	public ResponseEntity<HttpStatus> checkoutCart() {
		try {
			List<Cart> cart = new ArrayList<Cart>();

			cartRepo.findAll().forEach(cart::add);

			for (Cart temp : cart) {
				productRepo.updateStockProduct(temp.getQuantity(), temp.getCode());
			}

			cartRepo.deleteAll();

			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/cancel")
	public ResponseEntity<HttpStatus> cancelCart() {
		try {
			cartRepo.deleteAll();

			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
